FROM nvcr.io/nvidia/l4t-base:r32.5.0

ENV LANG=C.UTF-8
ENV OPENBLAS_CORETYPE=ARMV8

ENV PIP_VERSION_=21.2.2
ENV PYTHON_VERSION=3.6

# OS packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    python3-dev \
    build-essential \
    unzip \
    python3-setuptools \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*

# set default python version
RUN update-alternatives --install /usr/bin/python python /usr/bin/python${PYTHON_VERSION} 1 \
    && update-alternatives --install /usr/bin/python3 python3 /usr/bin/python${PYTHON_VERSION} 1

# install pip
RUN wget https://github.com/pypa/pip/archive/refs/tags/${PIP_VERSION_}.zip \
    && unzip ${PIP_VERSION_}.zip \
    && cd pip-${PIP_VERSION_} \
    && python setup.py install
