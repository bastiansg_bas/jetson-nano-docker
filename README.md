### Setup
#### Install requirements

This repo requires Docker 20.10 or above


#### Build & run image

```bash
$ cd <SUB_FOLDER>
$ make build
$ make run
```


#### Push image

```bash
$ cd <SUB_FOLDER>
$ make push
```
