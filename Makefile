REPO = registry.gitlab.com
IMAGE = bastiansg_bas/jetson-nano-docker/$(NAME)

login:
	docker login $(REPO)

build: check-parameters
	docker build -t $(REPO)/$(IMAGE):$(VERSION) -f ./$(NAME)/Dockerfile .
	docker tag $(REPO)/$(IMAGE):$(VERSION) $(REPO)/$(IMAGE):latest

run: check-parameters
	docker run -it --rm \
	--gpus all --privileged --network host \
	--volume /tmp/argus_socket:/tmp/argus_socket \
	--device /dev/video0 \
	$(REPO)/$(IMAGE):$(VERSION)

push: build login
	docker push $(REPO)/$(IMAGE):$(VERSION)
	docker push $(REPO)/$(IMAGE):latest

check-parameters:
ifndef NAME
	$(error parameter NAME is required)
endif

ifndef VERSION
	$(error parameter VERSION is required)
endif
