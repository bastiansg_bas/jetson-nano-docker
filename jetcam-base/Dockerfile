FROM nvcr.io/nvidia/dli/dli-nano-ai:v2.0.1-r32.4.4

ENV LANG=C.UTF-8
ENV OPENBLAS_CORETYPE=ARMV8

ENV PIP_VERSION_=21.2.2
ENV PYTHON_VERSION=3.6

# remove old jupyter-lab
RUN pip3 uninstall jupyterlab numpy -y \
    && rm -r ~/.jupyter

# remove old pip
RUN apt-get remove python3-pip -y

# OS packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    zsh \
    curl \
    unzip \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*

# set default python version
RUN update-alternatives --install /usr/bin/python python /usr/bin/python${PYTHON_VERSION} 1 \
    && update-alternatives --install /usr/bin/python3 python3 /usr/bin/python${PYTHON_VERSION} 1

WORKDIR /tmp

# install pip
RUN wget https://github.com/pypa/pip/archive/refs/tags/${PIP_VERSION_}.zip \
    && unzip ${PIP_VERSION_}.zip \
    && cd pip-${PIP_VERSION_} \
    && python setup.py install

# pip packages
COPY ./jetcam-base/requirements.txt /tmp
RUN pip install -r requirements.txt

WORKDIR /root
